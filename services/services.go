package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/final_exam2/api_gateway_service/config"
	"gitlab.com/final_exam2/api_gateway_service/genproto/organization_service"
	"gitlab.com/final_exam2/api_gateway_service/genproto/product_service"
	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
	"gitlab.com/final_exam2/api_gateway_service/genproto/warehouse_service"
)

type ServiceManagerI interface {
	// Organization Service
	BranchService() organization_service.BranchServiceClient
	MarketService() organization_service.MarketServiceClient
	ProviderService() organization_service.ProviderServiceClient
	StaffService() organization_service.StaffServiceClient

	// Product Service
	BrandService() product_service.BrandServiceClient
	CategoryService() product_service.CategoryServiceClient
	ProductService() product_service.ProductServiceClient

	//Warehouse Service
	RemainderService() warehouse_service.RemainderServiceClient
	ArrivalService() warehouse_service.ArrivalServiceClient
	ArrivalProductService() warehouse_service.ArrivalProductServiceClient

	//Sale Service
	ShiftService() sale_service.ShiftServiceClient
	PaymentService() sale_service.PaymentServiceClient
	TransactionService() sale_service.TransactionServiceClient
	SaleProductService() sale_service.SaleProductServiceClient
	SaleService() sale_service.SaleServiceClient
}

type grpcClients struct {
	// Organization Service

	branchService   organization_service.BranchServiceClient
	marketService   organization_service.MarketServiceClient
	providerService organization_service.ProviderServiceClient
	staffService    organization_service.StaffServiceClient

	// Product Service

	brandService    product_service.BrandServiceClient
	categoryService product_service.CategoryServiceClient
	productService  product_service.ProductServiceClient

	//Warehouse Service

	remainderService      warehouse_service.RemainderServiceClient
	arrivalService        warehouse_service.ArrivalServiceClient
	arrivalproductService warehouse_service.ArrivalProductServiceClient

	//sale Service

	shiftService       sale_service.ShiftServiceClient
	paymentService     sale_service.PaymentServiceClient
	transactionService sale_service.TransactionServiceClient
	saleproductService sale_service.SaleProductServiceClient
	saleService        sale_service.SaleServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Organization Microservice
	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationServiceHost+cfg.OrganizationGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	//Product Microservice

	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Warehouse Microservice

	connWarehouseService, err := grpc.Dial(
		cfg.WarehouseServiceHost+cfg.WarehouseGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Sale Microservice

	connSaleService, err := grpc.Dial(
		cfg.SaleServiceHost+cfg.SaleGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Organization  Service
		branchService:   organization_service.NewBranchServiceClient(connOrganizationService),
		marketService:   organization_service.NewMarketServiceClient(connOrganizationService),
		providerService: organization_service.NewProviderServiceClient(connOrganizationService),
		staffService:    organization_service.NewStaffServiceClient(connOrganizationService),

		// Product  Service
		brandService:    product_service.NewBrandServiceClient(connProductService),
		categoryService: product_service.NewCategoryServiceClient(connProductService),
		productService:  product_service.NewProductServiceClient(connProductService),

		// Product  Service
		remainderService:      warehouse_service.NewRemainderServiceClient(connWarehouseService),
		arrivalService:        warehouse_service.NewArrivalServiceClient(connWarehouseService),
		arrivalproductService: warehouse_service.NewArrivalProductServiceClient(connWarehouseService),

		// Product  Service
		shiftService:       sale_service.NewShiftServiceClient(connSaleService),
		paymentService:     sale_service.NewPaymentServiceClient(connSaleService),
		transactionService: sale_service.NewTransactionServiceClient(connSaleService),
		saleproductService: sale_service.NewSaleProductServiceClient(connSaleService),
		saleService:        sale_service.NewSaleServiceClient(connSaleService),
	}, nil
}

// Organization Service

func (g *grpcClients) BranchService() organization_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) MarketService() organization_service.MarketServiceClient {
	return g.marketService
}

func (g *grpcClients) ProviderService() organization_service.ProviderServiceClient {
	return g.providerService
}

func (g *grpcClients) StaffService() organization_service.StaffServiceClient {
	return g.staffService
}

// Product Service

func (g *grpcClients) BrandService() product_service.BrandServiceClient {
	return g.brandService
}

func (g *grpcClients) CategoryService() product_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}

//Warehouse Service

func (g *grpcClients) RemainderService() warehouse_service.RemainderServiceClient {
	return g.remainderService
}

func (g *grpcClients) ArrivalService() warehouse_service.ArrivalServiceClient {
	return g.arrivalService
}

func (g *grpcClients) ArrivalProductService() warehouse_service.ArrivalProductServiceClient {
	return g.arrivalproductService
}

//Sale Service

func (g *grpcClients) ShiftService() sale_service.ShiftServiceClient {
	return g.shiftService
}

func (g *grpcClients) PaymentService() sale_service.PaymentServiceClient {
	return g.paymentService
}

func (g *grpcClients) TransactionService() sale_service.TransactionServiceClient {
	return g.transactionService
}

func (g *grpcClients) SaleProductService() sale_service.SaleProductServiceClient {
	return g.saleproductService
}

func (g *grpcClients) SaleService() sale_service.SaleServiceClient {
	return g.saleService
}
