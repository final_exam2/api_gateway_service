package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	// "github.com/swaggo/swag/example/basic/docs"

	"gitlab.com/final_exam2/api_gateway_service/api/docs"
	_ "gitlab.com/final_exam2/api_gateway_service/api/docs"
	"gitlab.com/final_exam2/api_gateway_service/api/handler"
	"gitlab.com/final_exam2/api_gateway_service/config"
)

func SetUpApi(r *gin.Engine, h *handler.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(500))

	v1 := r.Group("/v1")
	v1.Use(h.AuthMiddleware())

	//Register

	r.POST("/register", h.Register)

	//Login

	r.POST("/login", h.Login)

	// Organization Services Api

	// branch api
	v1.POST("/branch", h.CreateBranch)
	v1.GET("/branch/:id", h.GetByIdBranch)
	v1.GET("branch", h.BranchGetList)
	v1.PUT("/branch/:id", h.BranchUpdate)
	v1.DELETE("/branch/:id", h.DeleteBranch)

	//market api
	v1.POST("/market", h.CreateMarket)
	v1.GET("/market/:id", h.GetByIdMarket)
	v1.GET("market", h.MarketGetList)
	v1.PUT("/market/:id", h.MarketUpdate)
	v1.DELETE("/market/:id", h.DeleteMarket)

	//provider Api
	v1.POST("/provider", h.CreateProvider)
	v1.GET("/provider/:id", h.GetByIdProvider)
	v1.GET("provider", h.ProviderGetList)
	v1.PUT("/provider/:id", h.ProviderUpdate)
	v1.DELETE("/provider/:id", h.DeleteProvider)

	//staff Api
	v1.POST("/staff", h.CreateStaff)
	v1.GET("/staff/:id", h.GetByIdStaff)
	v1.GET("staff", h.StaffGetList)
	v1.PUT("/staff/:id", h.StaffUpdate)
	v1.DELETE("/staff/:id", h.DeleteStaff)
	// Product Services Api

	// Brand api
	v1.POST("/brand", h.CreateBrand)
	v1.GET("/brand/:id", h.GetByIdBrand)
	v1.GET("brand", h.BrandGetList)
	v1.PUT("/brand/:id", h.BrandUpdate)
	v1.DELETE("/brand/:id", h.DeleteBrand)
	// Category api
	v1.POST("/category", h.CreateCategory)
	v1.GET("/category/:id", h.GetByIdCategory)
	v1.GET("category", h.CategoryGetList)
	v1.PUT("/category/:id", h.CategoryUpdate)
	v1.DELETE("/category/:id", h.DeleteCategory)
	//Product api
	v1.POST("/product", h.CreateProduct)
	v1.GET("/product/:id", h.GetByIdProduct)
	v1.GET("product", h.ProductGetList)
	v1.PUT("/product/:id", h.ProductUpdate)
	v1.DELETE("/product/:id", h.DeleteProduct)

	// Warehouse Services Api

	//Remainder api
	v1.POST("/remainder", h.CreateRemainder)
	v1.GET("/remainder/:id", h.GetByIdRemainder)
	v1.GET("remainder", h.RemainderGetList)
	v1.PUT("/remainder/:id", h.RemainderUpdate)
	v1.DELETE("/remainder/:id", h.DeleteRemainder)
	//Arrival api
	v1.POST("/arrival", h.CreateArrival)
	v1.GET("/arrival/:id", h.GetByIdArrival)
	v1.GET("arrival", h.ArrivalGetList)
	v1.PUT("/arrival/:id", h.ArrivalUpdate)
	v1.DELETE("/arrival/:id", h.DeleteArrival)
	//ArrivalProduct api
	v1.POST("/arrival_product", h.CreateArrivalProduct)
	v1.GET("/arrival_product/:id", h.GetByIdArrivalProduct)
	v1.GET("arrival_product", h.ArrivalProductGetList)
	v1.PUT("/arrival_product/:id", h.ArrivalProductUpdate)
	v1.DELETE("/arrival_product/:id", h.DeleteArrivalProduct)

	// Sale Services Api

	// Shift api
	v1.POST("/shift", h.CreateShift)
	v1.GET("/shift/:id", h.GetByIdShift)
	v1.GET("shift", h.ShiftGetList)
	v1.PUT("/shift/:id", h.ShiftUpdate)
	v1.DELETE("/shift/:id", h.DeleteShift)
	// MakeSale api
	v1.GET("/make_sale/:id", h.MakeSale)
	// MakeArrival api

	v1.GET("make_arrival/:id", h.MakeArrival)

	//openShift api

	v1.POST("/open_shift", h.OpenShift)

	//close_shift api

	v1.GET("/close_shift/:id", h.CloseShift)

	//  ScanBarCode

	// v1.GET("/scan_bar_code/:id", h.ScanBarCode)

	//Payment api
	v1.POST("/payment", h.CreatePayment)
	v1.GET("/payment/:id", h.GetByIdPayment)
	v1.GET("payment", h.PaymentGetList)
	v1.PUT("/payment/:id", h.PaymentUpdate)
	v1.DELETE("/payment/:id", h.DeletePayment)

	//Transaction api
	v1.POST("/transaction", h.CreateTransaction)
	v1.GET("/transaction/:id", h.GetByIdTransaction)
	v1.GET("transaction", h.TransactionGetList)
	v1.PUT("/transaction/:id", h.TransactionUpdate)
	v1.DELETE("/transaction/:id", h.DeleteTransaction)

	//SaleProduct api
	v1.POST("/sale_product", h.CreateSaleProduct)
	v1.GET("/sale_product/:id", h.GetByIdSaleProduct)
	v1.GET("sale_product", h.SaleProductGetList)
	v1.PUT("/sale_product/:id", h.SaleProductUpdate)
	v1.DELETE("/sale_product/:id", h.DeleteSaleProduct)

	//Sale api
	v1.POST("/sale", h.CreateSale)
	v1.GET("/sale/:id", h.GetByIdSale)
	v1.GET("sale", h.SaleGetList)
	v1.PUT("/sale/:id", h.SaleUpdate)
	v1.DELETE("/sale/:id", h.DeleteSale)

	// r.GET("/download-excel/:id", h.GetByIdGiveCar)

	url := ginSwagger.URL("swagger/doc.json")
	r.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
