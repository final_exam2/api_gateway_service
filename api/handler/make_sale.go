package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
	"gitlab.com/final_exam2/api_gateway_service/genproto/warehouse_service"
)
// @Security ApiKeyAuth
// MakeSale godoc
// @ID make_sale
// @Router /v1/make_sale/{id} [GET]
// @Summary Make Sale
// @Description Make Sale
// @Tags MakeSale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h Handler) MakeSale(ctx *gin.Context) {
	MakeSaleId := ctx.Param("id")

	sale, err := h.services.SaleService().GetById(ctx, &sale_service.SalePrimaryKey{Id: MakeSaleId})

	if err != nil {
		h.handlerResponse(ctx, "MakeSale=>h.services.SaleService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	sale_product, err := h.services.SaleProductService().GetList(ctx, &sale_service.SaleProductGetListRequest{})

	if err != nil {
		h.handlerResponse(ctx, "MakeSale=>h.services.SaleProductService().GetList", http.StatusBadRequest, err.Error())
		return
	}

	for _, value := range sale_product.SaleProducts {
		if value.SaleId == sale.Id {
			remainder, err := h.services.RemainderService().GetById(ctx, &warehouse_service.RemainderPrimaryKey{ProductId: value.ProductId})

			if err != nil {
				h.handlerResponse(ctx, "MakeSale=>h.services.RemainderService().GetById", http.StatusBadRequest, err.Error())
				return
			}

			_, err = h.services.RemainderService().Update(ctx, &warehouse_service.RemainderUpdate{
				BranchId:   remainder.BranchId,
				CategoryId: remainder.CategoryId,
				BrandId:    remainder.BrandId,
				ProductId:  remainder.ProductId,
				BarCode:    remainder.BarCode,
				Count:      remainder.Count - value.Count,
				Price:      remainder.Price,
			})
			if err != nil {
				h.handlerResponse(ctx, "MakeSale=>h.services.RemainderService().Update", http.StatusBadRequest, err.Error())
				return
			}

			payment, err := h.services.PaymentService().GetById(ctx, &sale_service.PaymentPrimaryKey{Id: sale.PaymentId})

			if err != nil {
				h.handlerResponse(ctx, "MakeSale=>h.services.PaymentService().GetById", http.StatusBadRequest, err.Error())
				return
			}

			shift, err := h.services.ShiftService().GetById(ctx, &sale_service.ShiftPrimaryKey{Id: sale.ShiftId})
			if err != nil {
				h.handlerResponse(ctx, "MakeSale=>h.services.ShiftService().GetById", http.StatusBadRequest, err.Error())
				return
			}

			transaction, err := h.services.TransactionService().GetById(ctx, &sale_service.TransactionPrimaryKey{Id: shift.TransactionId})
			if err != nil {
				h.handlerResponse(ctx, "MakeSale=>h.services.TransactionService().GetById", http.StatusBadRequest, err.Error())
				return
			}

			UpdatedCash := transaction.Cash + payment.Cash
			UpdatedUzcard := transaction.Uzcard + payment.Uzcard
			UpdatedPayme := transaction.Payme + payment.Payme
			UpdatedClick := transaction.Click + payment.Click
			UpdatedHumo := transaction.Humo + payment.Humo
			UpdatedApelsin := transaction.Apelsin + payment.Apelsin

			_, err = h.services.TransactionService().Update(ctx, &sale_service.TransactionUpdate{
				Id:      transaction.Id,
				Cash:    UpdatedCash,
				Uzcard:  UpdatedUzcard,
				Payme:   UpdatedPayme,
				Click:   UpdatedClick,
				Humo:    UpdatedHumo,
				Apelsin: UpdatedApelsin,
			})

			if err != nil {
				h.handlerResponse(ctx, "MakeSale=>h.services.TransactionService().Update", http.StatusBadRequest, err.Error())
				return
			}
		}
	}

	_, err = h.services.SaleService().Update(ctx, &sale_service.SaleUpdate{
		Id:        sale.Id,
		BranchId:  sale.BranchId,
		ShiftId:   sale.ShiftId,
		MarketId:  sale.MarketId,
		StaffId:   sale.StaffId,
		Status:    "finished",
		PaymentId: sale.PaymentId,
	})

	if err != nil {
		h.handlerResponse(ctx, "MakeSale=>h.services.SaleService().Update", http.StatusBadRequest, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, "Sale is made")
}
