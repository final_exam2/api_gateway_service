package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/final_exam2/api_gateway_service/genproto/product_service"
	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
	"gitlab.com/final_exam2/api_gateway_service/genproto/warehouse_service"
)

// import (
// 	"net/http"

// 	"github.com/gin-gonic/gin"
// 	"gitlab.com/final_exam2/api_gateway_service/genproto/product_service"
// 	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
// )

// @Security ApiKeyAuth
// ScanBarCode godoc
// @ID scan_bar_code
// @Router /v1/scan_bar_code/{id} [GET]
// @Summary ScanBarCode
// @Description Scan BarCode
// @Tags Scan BarCode
// @Accept json
// @Procedure json
// @Param sale_id path string true "sale_id"
// @Param bar_code path string true "bar_code"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ScanBarCode(ctx *gin.Context) {
	sale_id, err := h.getSaleIdParam(ctx)

	if err != nil {
		h.handlerResponse(ctx, "ScanBarCode---->getSaleIdParam", http.StatusBadRequest, err.Error())

		return
	}

	bar_code, err := h.getBarCodeParam(ctx)

	if err != nil {
		h.handlerResponse(ctx, "ScanBarCode---->getBarCodeParam", http.StatusBadRequest, err.Error())

		return
	}

	remainder, err := h.services.RemainderService().GetList(ctx, &warehouse_service.RemainderGetListRequest{BarCode: bar_code})
	if err != nil {
		h.handlerResponse(ctx, "ScanBarCode------>RemainderService().GetById", http.StatusBadRequest, err.Error())

		return
	}

	// sale_product, err := h.services.SaleProductService().GetList(ctx, &sale_service.SaleProductGetListRequest{SearchByBarCode: bar_code})

	// if err != nil {
	// 	h.handlerResponse(ctx, "ScanBarCode------>SaleProductService().GetList", http.StatusBadRequest, err.Error())

	// 	return
	// }
	// product, err := h.services.ProductService().GetById(ctx, &product_service.ProductPrimaryKey{Id: sale_product.SaleProducts[0].Id})

	// if err != nil {
	// 	h.handlerResponse(ctx, "ScanBarCode------>ProductService().GetById", http.StatusBadRequest, err.Error())

	// 	return
	// }

	// _, err = h.services.SaleService().GetList(ctx, &sale_service.SaleGetListRequest{SearchBySaleId: sale_id})

	// if err != nil {
	// 	h.handlerResponse(ctx, "ScanBarCode------>SaleService().GetList", http.StatusBadRequest, err.Error())

	// 	return
	// }

	// sale, err := h.services.SaleService().GetById(ctx, &sale_service.SalePrimaryKey{Id: sale_product.SaleProducts[0].SaleId})

	// if err != nil {
	// 	h.handlerResponse(ctx, "ScanBarCode------>SaleService().GetById", http.StatusBadRequest, err.Error())

	// 	return
	// }

	// if len(sale_product.SaleProducts) == 0 || len(sale_product.SaleProducts) < 0 {
	// 	_, err = h.services.SaleProductService().Create(ctx, &sale_service.SaleProductCreate{
	// 		BrandId:     product.BrandId,
	// 		ProductId:   product.Id,
	// 		BarCode:     product.BarCode,
	// 		RemainderId: product.Id,
	// 		Count:       1,
	// 		Price:       product.Price,
	// 		CategoryId:  product.CategoryId,
	// 		SaleId:      sale.Id,
	// 	})
	// 	if err != nil {
	// 		h.handlerResponse(ctx, "ScanBarCode------>SaleProductService().Create", http.StatusBadRequest, err.Error())

	// 		return

	// 	}
	// } else {
	// 	_, err = h.services.SaleProductService().Update(ctx, &sale_service.SaleProductUpdate{
	// 		Id:          sale_product.SaleProducts[0].Id,
	// 		BrandId:     sale_product.SaleProducts[0].BrandId,
	// 		ProductId:   sale_product.SaleProducts[0].ProductId,
	// 		BarCode:     sale_product.SaleProducts[0].BarCode,
	// 		RemainderId: sale_product.SaleProducts[0].RemainderId,
	// 		Count:       sale_product.SaleProducts[0].Count + 1,
	// 		Price:       sale_product.SaleProducts[0].Price,
	// 		CategoryId:  sale_product.SaleProducts[0].CategoryId,
	// 		SaleId:      sale_product.SaleProducts[0].SaleId,
	// 	})
	// 	if err != nil {
	// 		h.handlerResponse(ctx, "ScanBarCode------>SaleProductService().Update", http.StatusBadRequest, err.Error())
	// 		return
	// 	}

	// }

	//  <=============================================================================================================>
	//  <=============================================================================================================>
	//  <=============================================================================================================>
	//  <=============================================================================================================>
	//  <=============================================================================================================>
	//  <=============================================================================================================>
	//  <=============================================================================================================>
	//  <=============================================================================================================>

	if remainder.Count == 0 {
		product, err := h.services.ProductService().GetList(ctx, &product_service.ProductGetListRequest{SearchByBarCode: bar_code})
		if err != nil {
			h.handlerResponse(ctx, "ScanBarCode------>ProductService().GetList", http.StatusBadRequest, err.Error())

			return
		}

		for _, value := range product.Products {
			sale_products, err := h.services.SaleProductService().GetList(ctx, &sale_service.SaleProductGetListRequest{
				SearchByBarCode: bar_code,
			})

			if err != nil {
				h.handlerResponse(ctx, "ScanBarCode------>SaleProductService().GetList", http.StatusBadRequest, err.Error())

				return
			}

			if sale_products.Count == 0 {
				sales, err := h.services.SaleService().GetList(ctx, &sale_service.SaleGetListRequest{
					SearchBySaleId: sale_id,
				})
				if err != nil {
					h.handlerResponse(ctx, "ScanBarCode------>SaleService().GetList", http.StatusBadRequest, err.Error())

					return

				}
				for _, sale := range sales.Sales {
					_, err = h.services.SaleProductService().Create(ctx, &sale_service.SaleProductCreate{
						BrandId:     value.BrandId,
						ProductId:   value.Id,
						BarCode:     value.BarCode,
						RemainderId: value.Id,
						Count:       1,
						Price:       value.Price,
						CategoryId:  value.CategoryId,
						SaleId:      sale.Id,
					})
					if err != nil {
						h.handlerResponse(ctx, "ScanBarCode------>SaleProductService().Create", http.StatusBadRequest, err.Error())

						return

					}

				}

			} else {
				for _, v := range sale_products.SaleProducts {
					s_product, err := h.services.SaleProductService().GetById(ctx, &sale_service.SaleProductPrimaryKey{
						Id: v.Id,
					})
					if err != nil {
						h.handlerResponse(ctx, "ScanBarCode------>SaleProductService().GetByID", http.StatusBadRequest, err.Error())
						return
					}

					_, err = h.services.SaleProductService().Update(ctx, &sale_service.SaleProductUpdate{
						Id:          v.Id,
						BrandId:     v.BrandId,
						ProductId:   v.ProductId,
						BarCode:     v.BarCode,
						RemainderId: v.RemainderId,
						Count:       s_product.Count + 1,
						Price:       v.Price,
						CategoryId:  v.CategoryId,
						SaleId:      v.SaleId,
					})
					if err != nil {
						h.handlerResponse(ctx, "ScanBarCode------>SaleProductService().Update", http.StatusBadRequest, err.Error())
						return
					}

				}

			}
			sales, err := h.services.SaleService().GetList(ctx, &sale_service.SaleGetListRequest{SearchBySaleId: sale_id})
			if err != nil {
				h.handlerResponse(ctx, "ScanBarCode------>SaleService().GetList", http.StatusBadRequest, err.Error())
				return
			}
			for _, val := range sales.Sales {
				_, err := h.services.RemainderService().Create(ctx, &warehouse_service.RemainderCreate{
					BranchId:   val.BranchId,
					CategoryId: value.CategoryId,
					BrandId:    value.BrandId,
					ProductId:  value.Id,
					BarCode:    value.BarCode,
					Count:      1,
					Price:      value.Price,
				})

				if err != nil {
					h.handlerResponse(ctx, "ScanBarCode------>RemainderService().Create", http.StatusBadRequest, err.Error())
					return
				}

			}

		}
	}
}
