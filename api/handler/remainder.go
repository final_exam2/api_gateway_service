package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)
// @Security ApiKeyAuth
// Create remainder godoc
// @ID create_remainder
// @Router /v1/remainder [POST]
// @Summary Create Remainder
// @Description Create Remainder
// @Tags Remainder
// @Accept json
// @Procedure json
// @Param Remainder body warehouse_service.RemainderCreate true "CreateRemainderRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateRemainder(ctx *gin.Context) {
	var Remainder warehouse_service.RemainderCreate

	err := ctx.ShouldBind(&Remainder)
	if err != nil {
		h.handlerResponse(ctx, "CreateRemainder", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.RemainderService().Create(ctx, &Remainder)
	if err != nil {
		h.handlerResponse(ctx, "RemainderService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Remainder resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// GetByID remainder godoc
// @ID get_by_id_remainder
// @Router /v1/remainder/{id} [GET]
// @Summary Get By ID Remainder
// @Description Get By ID Remainder
// @Tags Remainder
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdRemainder(ctx *gin.Context) {
	RemainderId := ctx.Param("id")

	if !helper.IsValidUUID(RemainderId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.RemainderService().GetById(ctx, &warehouse_service.RemainderPrimaryKey{ProductId: RemainderId})
	if err != nil {
		h.handlerResponse(ctx, "RemainderService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Remainder resposne", http.StatusCreated, resp)
}
// @Security ApiKeyAuth
// GetList remainder godoc
// @ID get_list_remainder
// @Router /v1/remainder [GET]
// @Summary Get List Remainder
// @Description Get List Remainder
// @Tags Remainder
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search-By-Branch query string false "search-by-branch"
// @Param Search-By-Brand query string false "search-by-brand"
// @Param Search-By-Category query string false "search-by-category"
// @Param Search-By-BarCode query string false "search-by-bar-code"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) RemainderGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Remainder offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Remainder limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.RemainderService().GetList(ctx.Request.Context(), &warehouse_service.RemainderGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByBranch:   ctx.Query("search-by-branch"),
		SearchByBrand:    ctx.Query("search-by-brand"),
		SearchByCategory: ctx.Query("search-by-category"),
		SearchByBarCode:  ctx.Query("search-by-bar-code"),
	})
	if err != nil {
		h.handlerResponse(ctx, "RemainderService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Remainder resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// Update remainder godoc
// @ID update_remainder
// @Router /v1/remainder/{id} [PUT]
// @Summary Update Remainder
// @Description Update Remainder
// @Tags Remainder
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Remainder body warehouse_service.RemainderUpdate true "UpdateRemainderRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) RemainderUpdate(ctx *gin.Context) {
	var (
		id              string = ctx.Param("id")
		Remainderupdate warehouse_service.RemainderUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Remainderupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Remainder should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Remainderupdate.ProductId = id
	resp, err := h.services.RemainderService().Update(ctx.Request.Context(), &Remainderupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.RemainderService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Remainder resposne", http.StatusAccepted, resp)
}
// @Security ApiKeyAuth
// Delete remainder godoc
// @ID delete_remainder
// @Router /v1/remainder/{id} [DELETE]
// @Summary Delete Remainder
// @Description Delete Remainder
// @Tags Remainder
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteRemainder(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.RemainderService().Delete(c.Request.Context(), &warehouse_service.RemainderPrimaryKey{ProductId: id})
	if err != nil {
		h.handlerResponse(c, "services.RemainderService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Remainder resposne", http.StatusNoContent, resp)
}
