package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/organization_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)

// @Security ApiKeyAuth
// Create market godoc
// @ID create_market
// @Router /v1/market [POST]
// @Summary Create Market
// @Description Create Market
// @Tags Market
// @Accept json
// @Procedure json
// @Param Market body organization_service.MarketCreate true "CreateMarketRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateMarket(ctx *gin.Context) {
	var Market organization_service.MarketCreate

	err := ctx.ShouldBind(&Market)
	if err != nil {
		h.handlerResponse(ctx, "CreateMarket", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.MarketService().Create(ctx, &Market)
	if err != nil {
		h.handlerResponse(ctx, "MarketService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Market resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetByID market godoc
// @ID get_by_id_market
// @Router /v1/market/{id} [GET]
// @Summary Get By ID Market
// @Description Get By ID Market
// @Tags Market
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdMarket(ctx *gin.Context) {
	MarketId := ctx.Param("id")

	if !helper.IsValidUUID(MarketId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.MarketService().GetById(ctx, &organization_service.MarketPrimaryKey{Id: MarketId})
	if err != nil {
		h.handlerResponse(ctx, "MarketService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Market resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetList market godoc
// @ID get_list_market
// @Router /v1/market [GET]
// @Summary Get List Market
// @Description Get List Market
// @Tags Market
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MarketGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Market offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Market limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.MarketService().GetList(ctx.Request.Context(), &organization_service.MarketGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "MarketService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Market resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update market godoc
// @ID update_market
// @Router /v1/market/{id} [PUT]
// @Summary Update Market
// @Description Update Market
// @Tags Market
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Market body organization_service.MarketUpdate true "UpdateMarketRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MarketUpdate(ctx *gin.Context) {
	var (
		id           string = ctx.Param("id")
		Marketupdate organization_service.MarketUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Marketupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Market should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Marketupdate.Id = id
	resp, err := h.services.MarketService().Update(ctx.Request.Context(), &Marketupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.MarketService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Market resposne", http.StatusAccepted, resp)
}
// @Security ApiKeyAuth
// Delete market godoc
// @ID delete_market
// @Router /v1/market/{id} [DELETE]
// @Summary Delete Market
// @Description Delete Market
// @Tags Market
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteMarket(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.MarketService().Delete(c.Request.Context(), &organization_service.MarketPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.MarketService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Market resposne", http.StatusNoContent, resp)
}
