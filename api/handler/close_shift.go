package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)

// @Security ApiKeyAuth
// CloseShift godoc
// @ID close_shift
// @Router /v1/close_shift/{id} [GET]
// @Summary CloseShift
// @Description CloseShift
// @Tags CloseShift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CloseShift(ctx *gin.Context) {
	var (
		id string = ctx.Param("id")
		// Shiftupdate sale_service.ShiftUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	// err := ctx.ShouldBindJSON(&Shiftupdate)
	// if err != nil {
	// 	h.handlerResponse(ctx, "error Shift should bind json", http.StatusBadRequest, err.Error())
	// 	return
	// }

	shift, err := h.services.ShiftService().GetById(ctx, &sale_service.ShiftPrimaryKey{Id: id})

	if err != nil {
		h.handlerResponse(ctx, "CloseShift=>h.services.ShiftService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ShiftService().Update(ctx, &sale_service.ShiftUpdate{
		Id:            shift.Id,
		BranchId:      shift.BranchId,
		StaffId:       shift.StaffId,
		ProviderId:    shift.ProviderId,
		MarketId:      shift.MarketId,
		Status:        "closed",
		TransactionId: shift.TransactionId,
	})

	if err != nil {
		h.handlerResponse(ctx, "CloseShift=>h.services.ShiftService().Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, " shift closed", http.StatusAccepted, resp)
}
