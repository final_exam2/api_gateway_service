package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
)

// @Security ApiKeyAuth
// OpenShift godoc
// @ID open_shift
// @Router /v1/open_shift [POST]
// @Summary  Open Shift
// @Description Open Shift
// @Tags OpenShift
// @Accept json
// @Procedure json
// @Param Shift body sale_service.ShiftCreate true "CreateShiftRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) OpenShift(ctx *gin.Context) {
	var Shift sale_service.ShiftCreate

	err := ctx.ShouldBind(&Shift)
	if err != nil {
		h.handlerResponse(ctx, "OpenShift", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ShiftService().Create(ctx, &Shift)
	if err != nil {
		h.handlerResponse(ctx, "ShiftService().OpenShift", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "OpenShift Shift resposne", http.StatusOK, resp)
}
