package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)

// @Security ApiKeyAuth
// Create arrival_product godoc
// @ID create_arrival_product
// @Router /v1/arrival_product [POST]
// @Summary Create ArrivalProduct
// @Description Create ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Procedure json
// @Param ArrivalProduct body warehouse_service.ArrivalProductCreate true "CreateArrivalProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateArrivalProduct(ctx *gin.Context) {
	var ArrivalProduct warehouse_service.ArrivalProductCreate

	err := ctx.ShouldBind(&ArrivalProduct)
	if err != nil {
		h.handlerResponse(ctx, "CreateArrivalProduct", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ArrivalProductService().Create(ctx, &ArrivalProduct)
	if err != nil {
		h.handlerResponse(ctx, "ArrivalProductService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create ArrivalProduct resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetByID arrival_product godoc
// @ID get_by_id_arrival_product
// @Router /v1/arrival_product/{id} [GET]
// @Summary Get By ID ArrivalProduct
// @Description Get By ID ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdArrivalProduct(ctx *gin.Context) {
	ArrivalProductId := ctx.Param("id")

	if !helper.IsValidUUID(ArrivalProductId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.ArrivalProductService().GetById(ctx, &warehouse_service.ArrivalProductPrimaryKey{Id: ArrivalProductId})
	if err != nil {
		h.handlerResponse(ctx, "ArrivalProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id ArrivalProduct resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetList arrival_product godoc
// @ID get_list_arrival_product
// @Router /v1/arrival_product [GET]
// @Summary Get List ArrivalProduct
// @Description Get List ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search-By-Branch query string false "search-by-branch"
// @Param Search-By-Brand query string false "search-by-brand"
// @Param Search-By-Category query string false "search-by-category"
// @Param Search-By-BarCode query string false "search-by-bar-code"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ArrivalProductGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list ArrivalProduct offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list ArrivalProduct limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ArrivalProductService().GetList(ctx.Request.Context(), &warehouse_service.ArrivalProductGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByBranch:   ctx.Query("search-by-branch"),
		SearchByBrand:    ctx.Query("search-by-brand"),
		SearchByCategory: ctx.Query("search-by-category"),
		SearchByBarCode:  ctx.Query("search-by-bar-code"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ArrivalProductService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list ArrivalProduct resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update arrival_product godoc
// @ID update_arrival_product
// @Router /v1/arrival_product/{id} [PUT]
// @Summary Update ArrivalProduct
// @Description Update ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param ArrivalProduct body warehouse_service.ArrivalProductUpdate true "UpdateArrivalProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ArrivalProductUpdate(ctx *gin.Context) {
	var (
		id                   string = ctx.Param("id")
		ArrivalProductupdate warehouse_service.ArrivalProductUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&ArrivalProductupdate)
	if err != nil {
		h.handlerResponse(ctx, "error ArrivalProduct should bind json", http.StatusBadRequest, err.Error())
		return
	}

	ArrivalProductupdate.Id = id
	resp, err := h.services.ArrivalProductService().Update(ctx.Request.Context(), &ArrivalProductupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ArrivalProductService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create ArrivalProduct resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete arrival_product godoc
// @ID delete_arrival_product
// @Router /v1/arrival_product/{id} [DELETE]
// @Summary Delete ArrivalProduct
// @Description Delete ArrivalProduct
// @Tags ArrivalProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteArrivalProduct(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ArrivalProductService().Delete(c.Request.Context(), &warehouse_service.ArrivalProductPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ArrivalProductService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create ArrivalProduct resposne", http.StatusNoContent, resp)
}
