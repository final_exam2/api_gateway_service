package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/final_exam2/api_gateway_service/genproto/product_service"
	"gitlab.com/final_exam2/api_gateway_service/genproto/warehouse_service"
)
// @Security ApiKeyAuth
// MakeArrival godoc
// @ID make_arrival
// @Router /v1/make_arrival/{id} [GET]
// @Summary MakeArrival
// @Description Make Arrival
// @Tags Make Arrival
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MakeArrival(ctx *gin.Context) {
	var Arrival warehouse_service.ArrivalCreate
	ArrivalId := ctx.Param("id")

	err := ctx.ShouldBind(&Arrival)
	if err != nil {
		h.handlerResponse(ctx, "CreateArrival", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ArrivalService().GetById(ctx, &warehouse_service.ArrivalPrimaryKey{Id: ArrivalId})
	if err != nil {
		h.handlerResponse(ctx, "ArrivalService().GetById", http.StatusBadRequest, err.Error())

		return
	}

	arrival_product, err := h.services.ArrivalProductService().GetList(ctx, &warehouse_service.ArrivalProductGetListRequest{})

	if err != nil {
		h.handlerResponse(ctx, "ArrivalService=>h.services.ArrivalProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	for _, value := range arrival_product.ArrivalProducts {
		if value.ArrivalId == resp.Id {
			product, err := h.services.ProductService().GetById(ctx, &product_service.ProductPrimaryKey{Id: value.ProductId})

			if err != nil {
				h.handlerResponse(ctx, "ArrivalService=>h.services.ProductService().GetById", http.StatusBadRequest, err.Error())
				return
			}

			remainders, err := h.services.RemainderService().GetList(ctx, &warehouse_service.RemainderGetListRequest{})
			exists := false

			for _, val := range remainders.Remainders {
				if val.ProductId == value.ProductId {
					exists = true
				}
			}

			if exists == true {

				remainder, err := h.services.RemainderService().GetById(ctx, &warehouse_service.RemainderPrimaryKey{ProductId: value.ProductId})

				if err != nil {
					h.handlerResponse(ctx, "ArrivalService=>h.services.RemainderService().GetById", http.StatusBadRequest, err.Error())
					return
				}

				_, err = h.services.RemainderService().Update(ctx, &warehouse_service.RemainderUpdate{
					BranchId:   remainder.BranchId,
					CategoryId: remainder.CategoryId,
					BrandId:    remainder.BrandId,
					ProductId:  remainder.ProductId,
					BarCode:    remainder.BarCode,
					Count:      remainder.Count + value.Count,
					Price:      value.Price,
				})
				if err != nil {
					h.handlerResponse(ctx, "CreateArrival=>h.services.RemainderService().Update", http.StatusBadRequest, err.Error())
					return
				}
			} else {

				_, err = h.services.RemainderService().Create(ctx, &warehouse_service.RemainderCreate{
					BranchId:   resp.BranchId,
					CategoryId: value.CategoryId,
					BrandId:    value.BrandId,
					ProductId:  value.ProductId,
					BarCode:    value.BarCode,
					Count:      value.Count,
					Price:      value.Price,
				})

				if err != nil {
					h.handlerResponse(ctx, "CreateArrival=>h.services.RemainderService().Create", http.StatusBadRequest, err.Error())
					return

				}

			}

			_, err = h.services.ProductService().Update(ctx, &product_service.ProductUpdate{
				Id:         product.Id,
				Photo:      product.Photo,
				Name:       product.Name,
				CategoryId: product.CategoryId,
				BrandId:    product.BrandId,
				BarCode:    product.BarCode,
				Price:      value.Price,
			})

			if err != nil {
				h.handlerResponse(ctx, "CreateArrival=>h.services.ProductService().Update", http.StatusBadRequest, err.Error())
				return
			}
		}
	}

	h.handlerResponse(ctx, "create Arrival resposne", http.StatusOK, resp)
}
