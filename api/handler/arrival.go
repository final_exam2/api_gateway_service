package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)
// @Security ApiKeyAuth
// Create arrival godoc
// @ID create_arrival
// @Router /v1/arrival [POST]
// @Summary Create Arrival
// @Description Create Arrival
// @Tags Arrival
// @Accept json
// @Procedure json
// @Param Arrival body warehouse_service.ArrivalCreate true "CreateArrivalRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateArrival(ctx *gin.Context) {
	var Arrival warehouse_service.ArrivalCreate

	err := ctx.ShouldBind(&Arrival)
	if err != nil {
		h.handlerResponse(ctx, "CreateArrival", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ArrivalService().Create(ctx, &Arrival)
	if err != nil {
		h.handlerResponse(ctx, "ArrivalService().Create", http.StatusBadRequest, err.Error())

		return
	}
	h.handlerResponse(ctx, "create Arrival resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// GetByID arrival godoc
// @ID get_by_id_arrival
// @Router /v1/arrival/{id} [GET]
// @Summary Get By ID Arrival
// @Description Get By ID Arrival
// @Tags Arrival
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdArrival(ctx *gin.Context) {
	ArrivalId := ctx.Param("id")

	if !helper.IsValidUUID(ArrivalId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.ArrivalService().GetById(ctx, &warehouse_service.ArrivalPrimaryKey{Id: ArrivalId})
	if err != nil {
		h.handlerResponse(ctx, "ArrivalService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Arrival resposne", http.StatusCreated, resp)
}
// @Security ApiKeyAuth
// GetList arrival godoc
// @ID get_list_arrival
// @Router /v1/arrival [GET]
// @Summary Get List Arrival
// @Description Get List Arrival
// @Tags Arrival
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search-By-Branch query string false "search-by-branch"
// @Param Search-By-Arriva-Id query string false "search-by-arriva-id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ArrivalGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Arrival offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Arrival limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ArrivalService().GetList(ctx.Request.Context(), &warehouse_service.ArrivalGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByBranch:   ctx.Query("search-by-branch"),
		SearchByArrivaId: ctx.Query("search-by-arriva-id"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ArrivalService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Arrival resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// Update arrival godoc
// @ID update_arrival
// @Router /v1/arrival/{id} [PUT]
// @Summary Update Arrival
// @Description Update Arrival
// @Tags Arrival
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Arrival body warehouse_service.ArrivalUpdate true "UpdateArrivalRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ArrivalUpdate(ctx *gin.Context) {
	var (
		id            string = ctx.Param("id")
		Arrivalupdate warehouse_service.ArrivalUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Arrivalupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Arrival should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Arrivalupdate.Id = id
	resp, err := h.services.ArrivalService().Update(ctx.Request.Context(), &Arrivalupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ArrivalService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Arrival resposne", http.StatusAccepted, resp)
}
// @Security ApiKeyAuth
// Delete arrival godoc
// @ID delete_arrival
// @Router /v1/arrival/{id} [DELETE]
// @Summary Delete Arrival
// @Description Delete Arrival
// @Tags Arrival
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteArrival(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ArrivalService().Delete(c.Request.Context(), &warehouse_service.ArrivalPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ArrivalService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Arrival resposne", http.StatusNoContent, resp)
}
