package handler

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/final_exam2/api_gateway_service/api/models"
	"gitlab.com/final_exam2/api_gateway_service/genproto/organization_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
	"golang.org/x/crypto/bcrypt"
)

// Login godoc
// @ID login
// @Router /login [POST]
// @Summary Login
// @Description Login
// @Tags Login
// @Accept json
// @Procedure json
// @Param login body models.Login true "LoginRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) Login(ctx *gin.Context) {
	var login models.Login

	err := ctx.ShouldBindJSON(&login) // parse req body to given type struct
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.StaffService().GetById(context.Background(), &organization_service.StaffPrimaryKey{Login: login.Login})
	if err != nil {
		if err.Error() == "no rows in result set" {
			ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "error",
				"message": "User Does not exist",
			})
			return
		}
		fmt.Println(resp)


		
		ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	if login.UserType != resp.UserType {
		ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": "wrong user types!!!",
		})
		return
	}
	match := CheckPasswordHash(login.Password, resp.Password)
	if match == false {
		ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": "password does not match",
		})
		return
	}

	token, err := helper.GenerateJWT(map[string]interface{}{
		"user_id": resp.Id,
	}, time.Hour*360, h.cfg.SecretKey)
	if match == false {
		ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	ctx.SetCookie("token", token, 60*60*24, "/", "localhost", false, true)
	ctx.JSON(http.StatusOK, map[string]interface{}{
		"status":  "OK",
		"message": "Loged in successfully",
	})
}

// Register godoc
// @ID register
// @Router /register [POST]
// @Summary Register
// @Description Register
// @Tags Register
// @Accept json
// @Procedure json
// @Param register body organization_service.StaffCreate true "StaffCreateRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) Register(ctx *gin.Context) {

	var createUser organization_service.StaffCreate
	err := ctx.ShouldBindJSON(&createUser)
	if err != nil {
		h.handlerResponse(ctx, "ShouldBindJSON Login", http.StatusBadRequest, err.Error())
		return
	}
	h.services.StaffService().Create(ctx, &createUser)
	if len(createUser.Password) < 7 {
		ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": "password has to contain more than 7 symbols!!",
		})
		return
	}
	fmt.Println(createUser.Login)
	resp, err := h.services.StaffService().GetById(context.Background(), &organization_service.StaffPrimaryKey{Login: createUser.Login})
	if err != nil {
		if err.Error() == "no rows in result set" {
			resp, err = h.services.StaffService().Create(ctx, &createUser)
			if err != nil {
				ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
					"status":  "error",
					"message": err.Error(),
				})
				return
			}
		} else {
			ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "error",
				"message": "User Already exists, please log in",
			})
			return
		}
	}
	resp, err = h.services.StaffService().GetById(context.Background(), &organization_service.StaffPrimaryKey{Id: resp.Id})

	ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
		"status":  "Created",
		"message": "Registered",
	})
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
