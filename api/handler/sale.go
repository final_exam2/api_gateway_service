package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/organization_service"
	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)
// @Security ApiKeyAuth
// Create sale godoc
// @ID create_sale
// @Router /v1/sale [POST]
// @Summary Create Sale
// @Description Create Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param Sale body sale_service.SaleCreate true "CreateSaleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateSale(ctx *gin.Context) {
	var Sale sale_service.SaleCreate

	err := ctx.ShouldBind(&Sale)
	if err != nil {
		h.handlerResponse(ctx, "CreateSale", http.StatusBadRequest, err.Error())
		return
	}
	resp, err := h.services.SaleService().Create(ctx, &Sale)
	if err != nil {
		h.handlerResponse(ctx, "SaleService().Create", http.StatusBadRequest, err.Error())

		return
	}

	branch, err := h.services.BranchService().GetById(ctx, &organization_service.BranchPrimaryKey{Id: resp.BranchId})
	if err != nil {
		h.handlerResponse(ctx, "BranchService().GetById", http.StatusBadRequest, err.Error())

		return
	}

	count, err := h.services.SaleService().GetList(ctx, &sale_service.SaleGetListRequest{})
	if err != nil {
		return
	}

	if err != nil {
		h.handlerResponse(ctx, "SaleService().GetList", http.StatusBadRequest, err.Error())

		return
	}

	gent_id := helper.GenerateString(string(branch.Name[0]), int(count.Count)+1)

	_, err = h.services.SaleService().Update(ctx, &sale_service.SaleUpdate{

		Id:        resp.Id,
		BranchId:  resp.BranchId,
		ShiftId:   resp.ShiftId,
		MarketId:  resp.MarketId,
		StaffId:   resp.StaffId,
		Status:    resp.Status,
		PaymentId: resp.PaymentId,
		SaleId:    gent_id,
	})

	h.handlerResponse(ctx, "create Sale resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// GetByID sale godoc
// @ID get_by_id_sale
// @Router /v1/sale/{id} [GET]
// @Summary Get By ID Sale
// @Description Get By ID Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdSale(ctx *gin.Context) {
	SaleId := ctx.Param("id")

	if !helper.IsValidUUID(SaleId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.SaleService().GetById(ctx, &sale_service.SalePrimaryKey{Id: SaleId})
	if err != nil {
		h.handlerResponse(ctx, "SaleService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Sale resposne", http.StatusCreated, resp)
}
// @Security ApiKeyAuth
// GetList sale godoc
// @ID get_list_sale
// @Router /v1/sale [GET]
// @Summary Get List Sale
// @Description Get List Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search-Shift-Id query string false "search-shift-id"
// @Param Search-Staff query string false "search-staff"
// @Param Search-By-Sale-Id query string false "search-by-sale-id"
// @Param Search-By-Branch query string false "search-by-branch"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Sale offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Sale limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.SaleService().GetList(ctx.Request.Context(), &sale_service.SaleGetListRequest{
		Offset:         int64(offset),
		Limit:          int64(limit),
		SearchShiftId:  ctx.Query("search-shift-id"),
		SearchStaff:    ctx.Query("search-staff"),
		SearchBySaleId: ctx.Query("search-by-sale-id"),
		SearchBybranch: ctx.Query("search-by-branch"),
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Sale resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// Update sale godoc
// @ID update_sale
// @Router /v1/sale/{id} [PUT]
// @Summary Update Sale
// @Description Update Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Sale body sale_service.SaleUpdate true "UpdateSaleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleUpdate(ctx *gin.Context) {
	var (
		id         string = ctx.Param("id")
		Saleupdate sale_service.SaleUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Saleupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Sale should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Saleupdate.Id = id
	resp, err := h.services.SaleService().Update(ctx.Request.Context(), &Saleupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.SaleService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Sale resposne", http.StatusAccepted, resp)
}
// @Security ApiKeyAuth
// Delete sale godoc
// @ID delete_sale
// @Router /v1/sale/{id} [DELETE]
// @Summary Delete Sale
// @Description Delete Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteSale(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.SaleService().Delete(c.Request.Context(), &sale_service.SalePrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.SaleService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Sale resposne", http.StatusNoContent, resp)
}
