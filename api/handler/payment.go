package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)

// @Security ApiKeyAuth
// Create payment godoc
// @ID create_payment
// @Router /v1/payment [POST]
// @Summary Create Payment
// @Description Create Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param Payment body sale_service.PaymentCreate true "CreatePaymentRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreatePayment(ctx *gin.Context) {
	var Payment sale_service.PaymentCreate

	err := ctx.ShouldBind(&Payment)
	if err != nil {
		h.handlerResponse(ctx, "CreatePayment", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.PaymentService().Create(ctx, &Payment)
	if err != nil {
		h.handlerResponse(ctx, "PaymentService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Payment resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetByID payment godoc
// @ID get_by_id_payment
// @Router /v1/payment/{id} [GET]
// @Summary Get By ID Payment
// @Description Get By ID Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdPayment(ctx *gin.Context) {
	PaymentId := ctx.Param("id")

	if !helper.IsValidUUID(PaymentId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.PaymentService().GetById(ctx, &sale_service.PaymentPrimaryKey{Id: PaymentId})
	if err != nil {
		h.handlerResponse(ctx, "PaymentService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Payment resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetList payment godoc
// @ID get_list_payment
// @Router /v1/payment [GET]
// @Summary Get List Payment
// @Description Get List Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) PaymentGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Payment offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Payment limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.PaymentService().GetList(ctx.Request.Context(), &sale_service.PaymentGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "PaymentService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Payment resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update payment godoc
// @ID update_payment
// @Router /v1/payment/{id} [PUT]
// @Summary Update Payment
// @Description Update Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Payment body sale_service.PaymentUpdate true "UpdatePaymentRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) PaymentUpdate(ctx *gin.Context) {
	var (
		id            string = ctx.Param("id")
		Paymentupdate sale_service.PaymentUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Paymentupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Payment should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Paymentupdate.Id = id
	resp, err := h.services.PaymentService().Update(ctx.Request.Context(), &Paymentupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.PaymentService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Payment resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete payment godoc
// @ID delete_payment
// @Router /v1/payment/{id} [DELETE]
// @Summary Delete Payment
// @Description Delete Payment
// @Tags Payment
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeletePayment(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.PaymentService().Delete(c.Request.Context(), &sale_service.PaymentPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.PaymentService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Payment resposne", http.StatusNoContent, resp)
}
