package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/organization_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)

// @Security ApiKeyAuth
// Create staff godoc
// @ID create_staff
// @Router /v1/staff [POST]
// @Summary Create Staff
// @Description Create Staff
// @Tags Staff
// @Accept json
// @Procedure json
// @Param Staff body organization_service.StaffCreate true "CreateStaffRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateStaff(ctx *gin.Context) {
	var Staff organization_service.StaffCreate

	err := ctx.ShouldBind(&Staff)
	if err != nil {
		h.handlerResponse(ctx, "CreateStaff", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.StaffService().Create(ctx, &Staff)
	if err != nil {
		h.handlerResponse(ctx, "StaffService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Staff resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetByID staff godoc
// @ID get_by_id_staff
// @Router /v1/staff/{id} [GET]
// @Summary Get By ID Staff
// @Description Get By ID Staff
// @Tags Staff
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdStaff(ctx *gin.Context) {

	id := ctx.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.StaffService().GetById(ctx, &organization_service.StaffPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(ctx, "StaffService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Staff resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetList staff godoc
// @ID get_list_staff
// @Router /v1/staff [GET]
// @Summary Get List Staff
// @Description Get List Staff
// @Tags Staff
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) StaffGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Staff offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Staff limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.StaffService().GetList(ctx.Request.Context(), &organization_service.StaffGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "StaffService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Staff resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update staff godoc
// @ID update_staff
// @Router /v1/staff/{id} [PUT]
// @Summary Update Staff
// @Description Update Staff
// @Tags Staff
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Staff body organization_service.StaffUpdate true "UpdateStaffRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) StaffUpdate(ctx *gin.Context) {
	var (
		id          string = ctx.Param("id")
		Staffupdate organization_service.StaffUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Staffupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Staff should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Staffupdate.Id = id
	resp, err := h.services.StaffService().Update(ctx.Request.Context(), &Staffupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.StaffService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Staff resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete staff godoc
// @ID delete_staff
// @Router /v1/staff/{id} [DELETE]
// @Summary Delete Staff
// @Description Delete Staff
// @Tags Staff
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteStaff(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.StaffService().Delete(c.Request.Context(), &organization_service.StaffPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.StaffService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Staff resposne", http.StatusNoContent, resp)
}
