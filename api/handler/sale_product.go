package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)
// @Security ApiKeyAuth
// Create sale_product godoc
// @ID create_sale_product
// @Router /v1/sale_product [POST]
// @Summary Create SaleProduct
// @Description Create SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param SaleProduct body sale_service.SaleProductCreate true "CreateSaleProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateSaleProduct(ctx *gin.Context) {
	var SaleProduct sale_service.SaleProductCreate

	err := ctx.ShouldBind(&SaleProduct)
	if err != nil {
		h.handlerResponse(ctx, "CreateSaleProduct", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().Create(ctx, &SaleProduct)
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create SaleProduct resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// GetByID sale_product godoc
// @ID get_by_id_sale_product
// @Router /v1/sale_product/{id} [GET]
// @Summary Get By ID SaleProduct
// @Description Get By ID SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdSaleProduct(ctx *gin.Context) {
	SaleProductId := ctx.Param("id")

	if !helper.IsValidUUID(SaleProductId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.SaleProductService().GetById(ctx, &sale_service.SaleProductPrimaryKey{Id: SaleProductId})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id SaleProduct resposne", http.StatusCreated, resp)
}
// @Security ApiKeyAuth
// GetList sale_product godoc
// @ID get_list_sale_product
// @Router /v1/sale_product [GET]
// @Summary Get List SaleProduct
// @Description Get List SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search-By-Brand query string false "search-by-brand"
// @Param Search-By-Category query string false "search-by-category"
// @Param Search-By-BarCode query string false "search-by-bar-code"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list SaleProduct offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list SaleProduct limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.SaleProductService().GetList(ctx.Request.Context(), &sale_service.SaleProductGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByBrand:    ctx.Query("search-by-brand"),
		SearchByCategory: ctx.Query("search-by-category"),
		SearchByBarCode:  ctx.Query("search-by-bar-code"),
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list SaleProduct resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// Update sale_product godoc
// @ID update_sale_product
// @Router /v1/sale_product/{id} [PUT]
// @Summary Update SaleProduct
// @Description Update SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param SaleProduct body sale_service.SaleProductUpdate true "UpdateSaleProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductUpdate(ctx *gin.Context) {
	var (
		id                string = ctx.Param("id")
		SaleProductupdate sale_service.SaleProductUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&SaleProductupdate)
	if err != nil {
		h.handlerResponse(ctx, "error SaleProduct should bind json", http.StatusBadRequest, err.Error())
		return
	}

	SaleProductupdate.Id = id
	resp, err := h.services.SaleProductService().Update(ctx.Request.Context(), &SaleProductupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.SaleProductService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create SaleProduct resposne", http.StatusAccepted, resp)
}
// @Security ApiKeyAuth
// Delete sale_product godoc
// @ID delete_sale_product
// @Router /v1/sale_product/{id} [DELETE]
// @Summary Delete SaleProduct
// @Description Delete SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteSaleProduct(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.SaleProductService().Delete(c.Request.Context(), &sale_service.SaleProductPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.SaleProductService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create SaleProduct resposne", http.StatusNoContent, resp)
}
