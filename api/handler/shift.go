package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/final_exam2/api_gateway_service/genproto/sale_service"
	"gitlab.com/final_exam2/api_gateway_service/pkg/helper"
)
// @Security ApiKeyAuth
// Create shift godoc
// @ID create_shift
// @Router /v1/shift [POST]
// @Summary Create Shift
// @Description Create Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param Shift body sale_service.ShiftCreate true "CreateShiftRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CreateShift(ctx *gin.Context) {
	var Shift sale_service.ShiftCreate

	err := ctx.ShouldBind(&Shift)
	if err != nil {
		h.handlerResponse(ctx, "CreateShift", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ShiftService().Create(ctx, &Shift)
	if err != nil {
		h.handlerResponse(ctx, "ShiftService().Create", http.StatusBadRequest, err.Error())

		return
	}

	transaction, err := h.services.TransactionService().Create(ctx, &sale_service.TransactionCreate{
		Cash:    0,
		Uzcard:  0,
		Payme:   0,
		Click:   0,
		Humo:    0,
		Apelsin: 0,
	})

	if err != nil {
		h.handlerResponse(ctx, "Api--->Shift--->TransactionService().Create", http.StatusBadRequest, err.Error())

		return
	}
	_, err = h.services.ShiftService().Update(ctx, &sale_service.ShiftUpdate{
		Id:            resp.Id,
		BranchId:      resp.BranchId,
		ProviderId:    resp.ProviderId,
		MarketId:      resp.MarketId,
		Status:        resp.Status,
		TransactionId: transaction.Id,
		StaffId:       resp.StaffId,
	})

	if err != nil {
		h.handlerResponse(ctx, "Api--->Shift--->ShiftService().Update", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Shift resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// GetByID shift godoc
// @ID get_by_id_shift
// @Router /v1/shift/{id} [GET]
// @Summary Get By ID Shift
// @Description Get By ID Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) GetByIdShift(ctx *gin.Context) {
	ShiftId := ctx.Param("id")

	if !helper.IsValidUUID(ShiftId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.ShiftService().GetById(ctx, &sale_service.ShiftPrimaryKey{Id: ShiftId})
	if err != nil {
		h.handlerResponse(ctx, "ShiftService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Shift resposne", http.StatusCreated, resp)
}
// @Security ApiKeyAuth
// GetList shift godoc
// @ID get_list_shift
// @Router /v1/shift [GET]
// @Summary Get List Shift
// @Description Get List Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param Search-By-Shift-Id query string false "search-by-staff-id"
// @Param Search-By-Staff query string false "search-by-staff"
// @Param Search-By-Branch query string false "search-by-branch"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShiftGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Shift offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Shift limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ShiftService().GetList(ctx.Request.Context(), &sale_service.ShiftGetListRequest{
		Offset:          int64(offset),
		Limit:           int64(limit),
		SearchByShiftId: ctx.Query("search-by-staff-id"),
		SearchByStaff:   ctx.Query("search-by-staff"),
		SearchByBranch:  ctx.Query("search-by-branch"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ShiftService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Shift resposne", http.StatusOK, resp)
}
// @Security ApiKeyAuth
// Update shift godoc
// @ID update_shift
// @Router /v1/shift/{id} [PUT]
// @Summary Update Shift
// @Description Update Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Shift body sale_service.ShiftUpdate true "UpdateShiftRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShiftUpdate(ctx *gin.Context) {
	var (
		id          string = ctx.Param("id")
		Shiftupdate sale_service.ShiftUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Shiftupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Shift should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Shiftupdate.Id = id
	resp, err := h.services.ShiftService().Update(ctx.Request.Context(), &Shiftupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ShiftService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Shift resposne", http.StatusAccepted, resp)
}
// @Security ApiKeyAuth
// Delete shift godoc
// @ID delete_shift
// @Router /v1/shift/{id} [DELETE]
// @Summary Delete Shift
// @Description Delete Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) DeleteShift(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ShiftService().Delete(c.Request.Context(), &sale_service.ShiftPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ShiftService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Shift resposne", http.StatusNoContent, resp)
}
